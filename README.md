# QCoolLine

Небольшая программа, для рисования на сцене максимально коротких линий между двумя точками. Для нахождения кратчайшего пути использовался алгоритм поиска в ширину.

![alt text](https://bytebucket.org/Hakimov/qcoolline/raw/54ae876022e9721efeea3e3125f5541424b2fa65/screen.png "Logo Title Text 1")
